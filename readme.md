Phần 1: Test1/
Phần 2: getdata.html
----------------------------------------
Theo như kế hoạch trong chương trình training, chúng ta sẽ có bài kiểm tra về kiến thức các bạn đã học được trong thời gian vừa qua. Anh gởi mọi người nội dung của buổi kiểm tra này.
1. Mục đích: Kiểm tra kiến thức HTML, CSS, Javascript, Jquery, Bootstrap.
2. Thời gian: 
Thứ 6 ngày 21/10/2016
3. Yêu cầu:
Thời gian làm bài 8 tiếng
Bài làm được public lên bitbucket, gởi link lại đây khi hoàn thành (reply all)
4. Nội dung kiểm tra: gồm 2 phần
Phần 1: dựng trang web tĩnh, file test1 đính kèm
Phần 2: sử dụng ajax để load nội dung vào site, file test2 đính kèm
5. Đánh giá kết quả
Kết quả được đánh giá trong vòng 5 ngày kế tiếp